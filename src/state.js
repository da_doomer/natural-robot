import { writable } from 'svelte/store';
import { tweened } from 'svelte/motion';

export const robot = tweened({x: 0, y:0});

export const label_history = writable([]);
