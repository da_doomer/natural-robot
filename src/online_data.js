import { initializeApp } from 'firebase/app';
import { getDatabase } from 'firebase/database';
import { get } from 'firebase/database';
import { ref } from 'firebase/database';
import { push } from 'firebase/database';
import { child } from 'firebase/database';
import { update } from 'firebase/database';
import { onValue } from 'firebase/database';
import { Label } from "./algorithms.js";

const firebaseConfig = {
	apiKey: "AIzaSyDwVkCeyHubJ2lj7RMsjnLfSCoMlxOIFoo",
	authDomain: "natural-robot.firebaseapp.com",
	projectId: "natural-robot",
	storageBucket: "natural-robot.appspot.com",
	messagingSenderId: "822569384344",
	appId: "1:822569384344:web:6611162bffb535afa513ed"
};

let firebaseApp = initializeApp(firebaseConfig);
const database = getDatabase();

function with_online_label_history(callback) {
	let label_history_ref = ref(database, "label_history");
	get(label_history_ref).then((snapshot) => {
		let val = snapshot.val();
		if(val === null)
			return callback([]);
		let label_history = [];
		for(const record of Object.keys(val)) {
			let label = Label.from_json(val[record]);
			label_history = [...label_history, label];
		}
		callback(label_history);
	});
}

function get_new_id() {
	let id = push(child(ref(database), "label_history")).key;
	return id;
}

function push_label(label) {
	let id = label.id;
	let updates = {};
	updates["/label_history/" + id] = label;
	update(ref(database), updates);
	return id;
}

export {
	with_online_label_history,
	push_label,
	get_new_id
};
