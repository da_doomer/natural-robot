/**
 * Natural Language Processing models.
 */

const API_URL = "https://api-inference.huggingface.co/models/cointegrated/LaBSE-en-ru";
function header_maker() {
	return {
		'Accept': 'application/json',
		'Content-Type': 'application/json',
		'Authorization': "Bearer api_NuCblKummhDCHGvZstosLUtTYykKZKgkIg"
	};
}

/*
 * The vectors into which a language model encodes a sentence.
 *
 * This is also called "embedding" or "feature-extraction".
 */
async function encode_utterance(utterance) {
	let data = {
		inputs: utterance,
		wait_for_model: true
	};
	let response = await fetch(API_URL, {
		headers: header_maker(),
		method: "POST", 
		body: JSON.stringify(data)
	});
	let res = await response.json();
	return res[0];
}

let cache = {};
async function utterance2vec(utterance) {
	if(utterance in cache) return cache[utterance];
	let vs = await encode_utterance(utterance);
	let vec = [];
	let n = vs.length;
	for(let i = 0; i < vs[0].length; i++) {
		let val = 0.0;
		for(let j = 0; j < n; j++) {
			val += vs[j][i];
		}
		vec.push(val/n);
	}
	cache[utterance] = vec;
	return vec;
}

export {
	encode_utterance,
	utterance2vec
};
