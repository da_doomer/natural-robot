class State {
	constructor(utterance, x, y, utterance_vec) {
		this.utterance = utterance;
		this.x = x;
		this.y = y;
		this.utterance_vec = utterance_vec;
	}
}


class Label {
	constructor(state, action, id) {
		this.state = state;
		this.action = action;
		this.id = id;
	}
}


let RELATIVE_TYPE = "relative";
let ABSOLUTE_TYPE = "absolute";

class Action {
	/**
	 * Relative actions move the robot relative to its current location.
	 */
	constructor(x, y, type) {
		this.x = x;
		this.y = y;
		this.type = type;
	}

	/**
	 * Return a transformed deep-copy of the state.
	 */
	transformed(state) {
		if(this.type === RELATIVE_TYPE)
			return this.transformed_relative(state);
		if(this.type === ABSOLUTE_TYPE)
			return this.transformed_absolute(state);
	}

	/**
	 * Return a transformed deep-copy of the state.
	 */
	transformed_relative(state, type) {
		let new_state = {...state};
		new_state.x = Math.min(1.0, Math.max(0.0, state.x + this.x));
		new_state.y = Math.min(1.0, Math.max(0.0, state.y + this.y));
		return new_state;
	}

	/**
	 * Return a transformed deep-copy of the state.
	 */
	transformed_absolute(state) {
		let new_state = {...state};
		new_state.x = Math.min(1.0, Math.max(0.0, this.x));
		new_state.y = Math.min(1.0, Math.max(0.0, this.y));
		return new_state;
	}
}

State.from_json = function(obj) {
	let utterance = obj.utterance;
	let x = obj.x;
	let y = obj.y;
	let utterance_vec = obj.utterance_vec;
	return new State(utterance, x, y, utterance_vec);
}

Action.from_json = function(obj) {
	let x = obj.x;
	let y = obj.y;
	let type = obj.type;
	return new Action(x, y, type);
}

Label.from_json = function(obj) {
	let state = State.from_json(obj.state);
	let action = Action.from_json(obj.action);
	let id = obj.id;
	return new Label(state, action, id);
}

/**
 * Returns the L2 norm of the given vector.
 */
function norm(v) {
	let x = 0.0;
	for(const xi of v)
		x += Math.pow(xi, 2);
	return Math.sqrt(x);
}

function distance(v1, v2) {
	let diff = [...v1];
	for(let i = 0; i < v1.length; i++)
		diff[i] -= v2[i];
	return norm(diff);
}

function sim_coords(v1, v2) {
	return 1/(1+distance(v1, v2));
}

function sim_action(c1, c2) {
	if(c1.type !== c2.type)
		return 0.1;
	// TEMP
	if(c1.x !== c2.x || c1.y !== c2.y) return 0.1;
	return 1.0;
	//
	let v1 = [c1.x, c1.y];
	let v2 = [c2.x, c2.y];
	return sim_coords(v1, v2);
}

// Adapted from
// https://gist.github.com/cyphunk/6c255fa05dd30e69f438a930faeb53fe#gistcomment-3649882
function softmax(arr, temperature) {
	const weights = arr.map(l => l/temperature);
	const maxLogit = Math.max(...weights);
	const scores = weights.map(l => Math.exp(l - maxLogit));
	const denom = scores.reduce((a, b) => a + b);
	return scores.map(s => s / denom);
}

function sim_state(s1, s2, label_history) {
	let sim = sim_coords(s1.utterance_vec, s2.utterance_vec);
	console.log([s1.utterance, s2.utterance, sim]);
	return sim;
}

// This models
//
//      p(action | state, label history dataset)
//
//
//      P(a | s, D) [Bayes]= P(a, s | D)/P(s)
//
//      where
//
//      P(a, s | D)
//
//      is approximated by
//
//      sum[(a', s') in D] sim((a, s), (a', s') | D) P((a', s') | D)
//
//      where
//
//      sim((a, s), (a', s') | D)
//
//      is defined as
//
//      sim(a, a' | D)*sim(s, s' | D)
//
// In this case we define
//
//      sim(a, a' | D) = 1 if a == a' else 0
//
//      sim(s, s' | D) = |word2vec(s_utterance) - word2vec(s'_utterance)|
//
//      We assign equal similarity to all actions to test whether utterances
//      are enough to drive learning. Other approaches would have to deal with
//      "relative" vs "absolute" instructions.
//
//      The second similarity definition similarly focuses on the command and
//      not the position of the robot in which the command is given. This stems
//      from homogeneity of the map.
//
function posterior(action, state, label_history) {
	let l = 0.0;
	for(const label of label_history) {
		let past_state = label.state;
		let past_action = label.action;
		let sim_s = sim_state(past_state, state, label_history);
		let sim_c = sim_action(past_action, action, label_history);
		l += sim_s*sim_c;
	}
	return l;
}

function random_float(a, b) {
	return Math.random() * (a - b) + b;
}

// Credit: https://gist.github.com/engelen/fbce4476c9e68c52ff7e5c2da5c24a28
/**
 * Retrieve the array key corresponding to the largest element in the array.
 *
 * @param {Array.<number>} array Input array
 * @return {number} Index of array element with largest value
 */
function argMax(array) {
  return array.map((x, i) => [x, i]).reduce((r, a) => (a[0] > r[0] ? a : r))[1];
}

function random_item(items, weights, temperature) {
	// DEBUG
	let i = argMax(weights);
	return items[i];
	//
	let probabilities = softmax(weights, temperature);
	let v = random_float(0, 1);
	let sum = 0;
	for(let i = 0; i < weights.length; i++) {
		if(i == weights.length-1 || sum + probabilities[i+1] > v)
			return items[i];
		sum += probabilities[i];
	}
	return items[items.length-1];
}

function sample_actions(state, relative_budget, relative_scale, absolute_budget) {
	let actions = [];

	// Add relative actions
	for(let i = 0; i <= relative_budget; i++)
	for(let j = 0; j <= relative_budget; j++) {
		let x = 2*i/relative_budget - 1;
		let y = 2*j/relative_budget - 1;
		let action = new Action(x*relative_scale, y*relative_scale, RELATIVE_TYPE);
		actions.push(action);
	}

	// Add absolute actions
	for(let i = 0; i <= absolute_budget; i++)
	for(let j = 0; j <= absolute_budget; j++) {
		let x = i/absolute_budget;
		let y = j/absolute_budget;
		let action = new Action(x, y, ABSOLUTE_TYPE);
		actions.push(action);
	}
	return actions;
}

export {
	State,
	Action,
	Label,
	sim_coords,
	sample_actions,
	random_item,
	posterior,
	softmax
};
